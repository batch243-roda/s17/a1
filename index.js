/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:
const printUserDetails = () => {
  alert("Hi! Please add your fullname, age, and location");
  const fullName = prompt("What is your fullname?");
  const age = prompt("How old are you?");
  const location = prompt("Where do you live?");

  console.log(`Hello, ${fullName}`);
  console.log(`You are ${age} years old`);
  console.log(`Your live in ${location}`);
};

printUserDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:
const favBandOrArtist = function () {
  console.log(
    "My top 5 favorite bands or artist are: \n\t1. The The Red Jumpsuit Apparatus\n\t2. Secondhand Serenade\n\t3. Ben & Ben\n\t4. Parokya ni Edgar\n\t5. Eraserheads"
  );
};

favBandOrArtist();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:
const favMovies = function () {
  console.log("My Favorite Movies Are:")
  console.log("\t1. Avengers: Endgame (2019)");
  console.log("\tRotten tomatoes rating: 94%");
  console.log("\t2. Avengers: Infinity War (2019)");
  console.log("\tRotten tomatoes rating: 85%");
  console.log("\t3. Titanic (1997)");
  console.log("\tRotten tomatoes rating: 87%");
  console.log("\t4. Now You See Me (2013)");
  console.log("\tRotten tomatoes rating: 50%");
  console.log("\t5. The Matrix (1999)");
  console.log("\tRotten tomatoes rating: 88%");
};
favMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
// let printFriends =
function printUsers() {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
}

// console.log(friend1);
// console.log(friend2);
